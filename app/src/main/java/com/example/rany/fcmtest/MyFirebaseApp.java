package com.example.rany.fcmtest;

import android.app.Application;

import com.google.firebase.messaging.FirebaseMessaging;

public class MyFirebaseApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // when user download our app, it will auto subscribe to news
        FirebaseMessaging.getInstance().subscribeToTopic("news");
    }
}
