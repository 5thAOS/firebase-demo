package com.example.rany.fcmtest.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.rany.fcmtest.DetailActivity;
import com.example.rany.fcmtest.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseIDReceiveService extends FirebaseMessagingService{

    private static final String TAG = "ooooo";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        // get token
        Log.e(TAG, "onNewToken: "+ s);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String title = remoteMessage.getNotification().getTitle();
        String body = remoteMessage.getNotification().getBody();
        Log.e(TAG, "onMessageReceived: "+ title);

        String id = null, artcle = null;
        if(remoteMessage.getData().size() > 0){
            id = remoteMessage.getData().get("id");
            artcle = remoteMessage.getData().get("article");
            Log.e(TAG, id + " , "+ artcle);
        }

        Intent i = new Intent(this, DetailActivity.class);
        i.putExtra("id", id);
        i.putExtra("article", artcle);

        PendingIntent intent = PendingIntent.getActivity(this,
                1, i, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationChannel channel = new NotificationChannel("channel_1",
                "my_channel", NotificationManager.IMPORTANCE_DEFAULT);
        NotificationManager cManager =
                (NotificationManager) getApplication().getSystemService(
                        Context.NOTIFICATION_SERVICE);
        cManager.createNotificationChannel(channel);

        NotificationCompat.Builder builder;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            builder = new NotificationCompat.Builder(this, "channel_1");
        }
        else{
            builder = new NotificationCompat.Builder(this);
        }
        builder.setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_notifications_active_black_24dp)
                .setContentIntent(intent);
        NotificationManager manager =
                (NotificationManager) getApplication().getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(1, builder.build());
    }
}
